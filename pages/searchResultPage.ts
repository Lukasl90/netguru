import { by, element, ElementFinder } from 'protractor';

export class SearchResultPageObject {
    public releasesSection: ElementFinder = element(by.css('div.bucket.releases'));
    public tracksSection: ElementFinder = element(by.css('div.bucket.tracks'));
}