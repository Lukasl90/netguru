import { by, element, ElementFinder } from 'protractor';

export class GenreDetailsPageObject {
    public genreHeader: ElementFinder = element(by.css('div.interior-title > h1'));
    public genrePageSelectors: ElementFinder = element(by.css('ul.page-selectors'));
}