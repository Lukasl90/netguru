import { by, element, ElementFinder } from 'protractor';

export class CommonPageObject {
    public searchField: ElementFinder = element(by.css('input.head-search-input.tt-input'));
    public mainMenuBar: ElementFinder = element(by.css('div.header-bg-wrap'));
    public loadingIcon: ElementFinder = element(by.css('.logo-icon-loading'));
    public genresBtn: ElementFinder = element(by.css('div.nav-links > ul > li.genre-parent.head-parent'));
    public genresList: ElementFinder = element(by.css(
        'div.genres-drop.head-drop.header-tooltip-menu > div#custom-genre-list + div.genre-drop-list'));

    public async chooseGenre(genre: string): Promise<void> {
        const genreBtn: ElementFinder = element(by.cssContainingText(
            `div.genres-drop.head-drop.header-tooltip-menu > div#custom-genre-list + div.genre-drop-list
             > ul > li > a`, genre));
        await genreBtn.click();
    }
}