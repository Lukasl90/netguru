import { by, element, ElementFinder } from 'protractor';

export class WelcomePageObject {
    public closeWelcomePopupBtn: ElementFinder = element(by.css('#bx-element-831086-epINrnK > button'));
}