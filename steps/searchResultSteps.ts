import { expect } from 'chai';
import { Given } from 'cucumber';
import { SearchResultPageObject } from '../pages/searchResultPage';
import { waitForElement } from '../support/supportMethods';

const searchResultPage: SearchResultPageObject = new SearchResultPageObject();

Given(/^I should see search results$/, async () => {
    await waitForElement(searchResultPage.tracksSection);
    expect(await searchResultPage.releasesSection.isDisplayed()).to.be
    .equal(true, 'Releases section is not displayed');
    expect(await searchResultPage.tracksSection.isDisplayed()).to.be
    .equal(true, 'Tracks section is not displayed');
});