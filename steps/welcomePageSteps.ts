import { expect } from 'chai';
import { Given, When } from 'cucumber';
import { browser } from 'protractor';
import { welcomePageURL } from '../data/urls';
import { CommonPageObject } from '../pages/commonPage';
import { WelcomePageObject } from '../pages/welcomePage';
import { clickBtn } from '../support/supportMethods';

const welcomePage: WelcomePageObject = new WelcomePageObject();
const commonPage: CommonPageObject = new CommonPageObject();

Given(/^I open welcome page$/, async () => {
    await browser.get(welcomePageURL);

    return expect(await browser.getCurrentUrl()).to.be.equal(welcomePageURL, 'URL is not correct');
});

When(/^I close welcome popup$/, async () => {
    try {
        expect(await welcomePage.closeWelcomePopupBtn.isDisplayed()).to.be
        .equal(true, 'welcome popup is not visible');

        return await clickBtn(welcomePage.closeWelcomePopupBtn);
    } catch {
        expect(await commonPage.mainMenuBar.isDisplayed()).to.be
        .equal(true, 'main menu bar is not visible');
    }
});