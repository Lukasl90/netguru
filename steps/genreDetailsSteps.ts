import { expect } from 'chai';
import { When } from 'cucumber';
import { GenreDetailsPageObject } from '../pages/genreDetailsPage';
import { checkPresenceOfText, waitForElement } from '../support/supportMethods';

const genreDetailsPage: GenreDetailsPageObject = new GenreDetailsPageObject();

When(/^I should see genre details for (.*?)$/, async (genreName: string) => {
    await waitForElement(genreDetailsPage.genreHeader);
    expect(await genreDetailsPage.genreHeader.isDisplayed()).to.be.equal(true, 'genre header is not visible');
    await checkPresenceOfText(genreDetailsPage.genreHeader, genreName);
    expect(await genreDetailsPage.genrePageSelectors.isDisplayed()).to.be
    .equal(true, 'page selectors on genre page are not visible');
});