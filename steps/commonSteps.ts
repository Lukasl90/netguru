import { expect } from 'chai';
import { When } from 'cucumber';
import { browser, protractor } from 'protractor';
import { CommonPageObject } from '../pages/commonPage';
import { SearchResultPageObject } from '../pages/searchResultPage';
import { sendText, waitForElement } from '../support/supportMethods';

const commonPage: CommonPageObject = new CommonPageObject();
const searchResultPage: SearchResultPageObject = new SearchResultPageObject();

When(/^I search for (.*?)$/, async (djName: string) => {
    expect(await commonPage.searchField.isDisplayed()).to.be.equal(true, 'search field is not visible');
    await sendText(commonPage.searchField, djName);
    await browser.actions().sendKeys(protractor.Key.ENTER).perform();
    try {
        await waitForElement(commonPage.loadingIcon);
    } catch {
        await waitForElement(searchResultPage.tracksSection);
    }
    expect(await browser.getCurrentUrl()).to.be
    .equal(`https://www.beatport.com/search?q=${djName}`, 'URL is not correct');
});

When(/^I hover over "Genres" button in main menu$/, async () => {
    await browser.actions().mouseMove(commonPage.genresBtn).perform();
    await waitForElement(commonPage.genresList);
    expect(await commonPage.genresList.isDisplayed()).to.be.equal(true, 'genres list is not visible');
});

When(/^I choose (.*?)$/, async (genreName: string) => {
    await waitForElement(commonPage.genresList);
    await commonPage.chooseGenre(genreName);
});