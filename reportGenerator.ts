const reporter = require('cucumber-html-reporter');

const options = {
    jsonFile: './reports/report.json',
    launchReport: true,
    output: './reports/report.html',
    reportSuiteAsScenarios: true,
    screenshotsDirectory: 'screenshots/',
    storeScreenshots: true,
    theme: 'bootstrap'
    };
reporter.generate(options);