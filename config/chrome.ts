import {Config} from 'protractor';

export const config: Config = {
    /* there is some EPIPE Error on OSX when control flow is turned off -
    https://github.com/angular/protractor/issues/4294 */
    SELENIUM_PROMISE_MANAGER: false,
    allScriptsTimeout: 30 * 1000,
    baseUrl: 'https://www.beatport.com/',
    capabilities: {
        acceptInsecureCerts: true,
        browserName: 'chrome',
        chromeOptions: {
            args: [ '--headless', '--disable-gpu', '--window-size=1280,1024' ]
            // args: [ '--window-size=1280,1024' ]
        }
    },
    cucumberOpts: {
        format: [
            'progress-bar',
            'json:./reports/report.json',
            'rerun:./reports/failedTests.md'
        ],
        keepAlive: false,
        require: [
            '../support/hooks.js',
            '../steps/*.js',
        ],
    },
    directConnect: true,
    framework: 'custom',
    frameworkPath: require.resolve('protractor-cucumber-framework'),
    ignoreUncaughtExceptions: true,
    specs: [
        '../../features/*.feature',
    ],
};