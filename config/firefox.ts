import {Config} from 'protractor';

export const config: Config = {
    SELENIUM_PROMISE_MANAGER: false,
    allScriptsTimeout: 30 * 1000,
    baseUrl: 'https://www.beatport.com/',
    capabilities: {
        'browserName': 'firefox',
        'moz:firefoxOptions': {
            args: [ '--headless' ]
          }
    },
    cucumberOpts: {
        format: [
            'progress-bar',
            'json:./reports/report.json',
            'rerun:./reports/failedTests.md'
        ],
        keepAlive: false,
        require: [
            '../support/hooks.js',
            '../steps/*.js',
        ],
    },
    directConnect: true,
    framework: 'custom',
    frameworkPath: require.resolve('protractor-cucumber-framework'),
    ignoreUncaughtExceptions: true,
    specs: [
        '../../features/*.feature',
    ]
};
