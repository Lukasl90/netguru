import { browser, ElementArrayFinder, ElementFinder } from 'protractor';

export async function clickBtn(btn: ElementFinder): Promise<void> {

    return await btn.click();
}

export async function sendText(inputField: ElementFinder, textToSend: string | number): Promise<void> {

    return await inputField.sendKeys(textToSend);
}

export async function checkPresenceOfText(
    elementsToGetText: ElementFinder, textToSearch: string): Promise<boolean> {
    const texts: string = await elementsToGetText.getText();

    return await texts.includes(textToSearch);
}

export async function waitForElement(el: ElementFinder): Promise<{}> {
    const EC: any = browser.ExpectedConditions;
    const timeout: number = 5000;

    return await browser.wait(EC.visibilityOf(el), timeout, 'element not visible');
}
