import {After, Before, setDefaultTimeout} from 'cucumber';
import {writeFileSync} from 'fs';
import {browser} from 'protractor';

setDefaultTimeout(30 * 1000);

Before(async () => {
    browser.waitForAngularEnabled(false);
    await browser.get('/');
    await clearSession();

    return await browser.get('/');
});

After(async (testResult: any) => {
    await saveScreenshots(testResult);

    return await clearSession();
});

async function clearSession() {
    await browser.executeScript('window.sessionStorage.clear();');
    await browser.executeScript('window.localStorage.clear();');

    return await browser.manage().deleteAllCookies();
}

async function saveScreenshots(testResult: any) {
    if (testResult.result.status === 'failed') {
        const fileName = './screenshots/' +
        testResult.sourceLocation.uri.split('features/')[1] + '_line_' +
        testResult.sourceLocation.line + '.png';
        const png: any = await browser.takeScreenshot();

        return writeFileSync(fileName, png, {encoding: 'base64'});
    }
}
