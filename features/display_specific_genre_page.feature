@genres
Feature: Test genres section on Beatport webpage

Scenario Outline: Open Beatport page and search for specific DJ
  Given I open welcome page
  When I close welcome popup
  And I hover over "Genres" button in main menu
  And I choose <genre name>
  Then I should see genre details for <genre name>
  Examples:
  | genre name  |
  | Dubstep     |
