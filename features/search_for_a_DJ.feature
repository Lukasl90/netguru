@search
Feature: Test search mechanisme on Beatport webpage

Scenario Outline: Open Beatport page and search for specific DJ
  Given I open welcome page
  When I close welcome popup
  And I search for <DJ name>
  Then I should see search results
  Examples:
  | DJ name   |
  | Skrillex  |
