# Netguru

Automation tests will validate key features of the Beatport webpage: https://www.beatport.com
1. Displaying details of the music genre
2. Searching for a DJ.

# Configuration:
1. run ```npm install```
2. run ```npm run webdrive-update```
3. if launching tests on OSX run ```npm run install-patch```
/* there is issue with EPIPE Error on OSX with async/await and control flow turned off -
    https://github.com/angular/protractor/issues/4294 */

# Running tests:

### Run the entire test suite:
```BROWSER={chrome | firefox} npm run test```
or with default browser = Google Chrome:
```npm run test```

#### Run specific test with tag:
```npm run test -- --cucumberOpts.tags="{@tag}"```

### Generate html report
You need to have *report.json* file. (it will be generated after runing some test)
```npm run report```

#### Lint .ts files
```npm run tslint```

#### Clear temporary files
```npm run clean```